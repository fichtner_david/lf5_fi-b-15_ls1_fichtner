package eingabeAusgabe;
import java.util.Scanner;
public class meFu {
   
	public static void main(String[] args) {

		// (E) "Eingabe"
		// Wert f�r x festlegen:
		// ==========================	
		textA();
		double x = eingabe();
		
		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		double ergebnis = rechnung(x);

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		ergebnisA(x, ergebnis);
	}
	
	public static double rechnung( double x) {

		double ergebnis= x * x;
		return ergebnis;
		
	}
	
	public static void textA() {

		System.out.println("Dieses Programm berechnet die Quadratzahl x�");
		System.out.println("---------------------------------------------");
		
	}
	
	public static void ergebnisA( double x,double ergebnis) {

		System.out.printf("x = %.2f und x�= %.2f\n", x, ergebnis);
		
	}
	
	public static double eingabe() {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Welche Zahl m�chteen Sie quadrieren?");
		double x = myScanner.nextDouble();
		return x;
		
	}
	
}
