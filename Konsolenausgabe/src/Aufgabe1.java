//David Fichtner
public class Aufgabe1 {

	public static void main(String[] args) {
		//Aufgabe 1
		System.out.print("Das ist ein Beispielsatz. ");
		System.out.print("Ein Beispielsatz ist das.");
		System.out.println("Das ist ein “Beispielsatz“.\r\n" + "Ein Beispielsatz ist das.");
		System.out.println("");
		//println geht automatisch in eine neue Zeile
		//Aufgabe 2
		System.out.printf("%7s\n", "*");
		System.out.printf("%8s\n", "***");
		System.out.printf("%9s\n", "*****");
		System.out.printf("%10s\n", "*******");
		System.out.printf("%11s\n", "*********");
		System.out.printf("%12s\n", "***********");
		System.out.printf("%13s\n", "*************");
		System.out.printf("%8s\n", "***");
		System.out.printf("%8s\n", "***");
		System.out.println("");
		//Aufgabe 3
		double a=22.4234234;      //nur zum zeigen
		System.out.printf("%.2f\n" , a);
		System.out.printf("%.2f\n" ,111.2222);
		System.out.printf("%.2f\n" ,4.0);
		System.out.printf("%.2f\n" ,1000000.551);
		System.out.printf("%.2f\n" ,97.34);

	}
}
