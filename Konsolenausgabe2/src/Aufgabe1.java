//David Fichtner
public class Aufgabe1 {

	public static void main(String[] args) {
		//Aufgabe 1
		System.out.printf("%5s\n", "**");
		System.out.printf("%s%7s\n", "*","*");
		System.out.printf("%s%7s\n", "*","*");
		System.out.printf("%5s\n", "**");
		
		//Aufgabe 2
		System.out.println(""); //die 3 Zeilen sind nur zum verschönern
		System.out.println("");
		System.out.println("");
		
		
		System.out.printf("%-5s", "0!");
		System.out.printf("%s", "=");
		System.out.printf("%-19s", "");
		System.out.printf("%s", "=");
		System.out.printf("%4s\n", "1");
		System.out.printf("%-5s", "1!");
		System.out.printf("%s", "=");
		System.out.printf("%-19s", "1");
		System.out.printf("%s", "=");
		System.out.printf("%4s\n", "1");
		System.out.printf("%-5s", "2!");
		System.out.printf("%s", "=");
		System.out.printf("%-19s", "1 * 2");
		System.out.printf("%s", "=");
		System.out.printf("%4s\n", "2");
		System.out.printf("%-5s", "3!");
		System.out.printf("%s", "=");
		System.out.printf("%-19s", "1 * 2 * 3");
		System.out.printf("%s", "=");
		System.out.printf("%4s\n", "6");
		System.out.printf("%-5s", "4!");
		System.out.printf("%s", "=");
		System.out.printf("%-19s", "1 * 2 * 3 * 4");
		System.out.printf("%s", "=");
		System.out.printf("%4s\n", "24");
		System.out.printf("%-5s", "5!");
		System.out.printf("%s", "=");
		System.out.printf("%-19s", "1 * 2 * 3 * 4 * 5");
		System.out.printf("%s", "=");
		System.out.printf("%4s\n", "120");
		
		//Aufgabe 3
		System.out.println("");
		System.out.println("");
		System.out.println("");
		
		
		System.out.printf("%-11s", "Fahrenheit");
		System.out.printf("%s", "|");
		System.out.printf("%10s\n", "Celsius");
		System.out.printf("%.22s\n", "-----------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-11s", "-20");
		System.out.printf("%s", "|");
		System.out.printf("%10s\n", "-28.89");
		System.out.printf("%-11s", "-10");
		System.out.printf("%s", "|");
		System.out.printf("%10s\n", "-23.33");
		System.out.printf("%-11s", "+0");
		System.out.printf("%s", "|");
		System.out.printf("%10s\n", "-17.78");
		System.out.printf("%-11s", "+20");
		System.out.printf("%s", "|");
		System.out.printf("%10s\n", "-6.67");
		System.out.printf("%-11s", "+30");
		System.out.printf("%s", "|");
		System.out.printf("%10s\n", "-1.11");
		
		// Diese Version finde ich besser
		System.out.println("");
		System.out.println("");
		System.out.println("");
		
		System.out.printf("%-11s|", "Fahrenheit");
		System.out.printf("%10s\n", "Celsius");
		System.out.printf("%.22s\n", "-----------|----------------------------------------------------------------------------------------------------");
		System.out.printf("%-11s|", "-20");
		System.out.printf("%10s\n", "-28.89");
		System.out.printf("%-11s|", "-10");
		System.out.printf("%10s\n", "-23.33");
		System.out.printf("%-11s|", "+0");
		System.out.printf("%10s\n", "-17.78");
		System.out.printf("%-11s|", "+20");
		System.out.printf("%10s\n", "-6.67");
		System.out.printf("%-11s|", "+30");
		System.out.printf("%10s\n", "-1.11");
		
		//Jetzt etwas eingepackt
		System.out.println("");
		System.out.println("");
		System.out.println("");
		
		
		System.out.printf("%-11s|%10s\n", "Fahrenheit", "Celsius");
		
		System.out.printf("%.22s\n", "-----------|----------------------------------------------------------------------------------------------------");
		System.out.printf("%-11s|%10s\n", "-20", "-28.89");		
		System.out.printf("%-11s|%10s\n", "-10", "-23.33");
		System.out.printf("%-11s|%10s\n", "+0", "-17.78");
		System.out.printf("%-11s|%10s\n", "+20", "-6.67");
		System.out.printf("%-11s|%10s\n", "+30", "-1.11");
	}
}
