package eingabeAusgabe;

import java.util.Scanner;

//David Fichtner
public class Aufgabe1 {

	public static void main(String[] args) {
	
		// Neues Scanner-Objekt myScanner wird erstellt
		 Scanner myScanner = new Scanner(System.in);

		 System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

		 // Die Variable zahl1 speichert die erste Eingabe
		 int zahl1 = myScanner.nextInt();

		 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

		 // Die Variable zahl2 speichert die zweite Eingabe
		 int zahl2 = myScanner.nextInt();

		 // Die Addition der Variablen zahl1 und zahl2
		 // wird der Variable ergebnis zugewiesen.
		 int ergebnisPl = zahl1 + zahl2;
		 int ergebnisMi = zahl1 - zahl2;
		 int ergebnisMa = zahl1 * zahl2;
		 int ergebnisDu = zahl1 / zahl2;

		 System.out.print("\n\n\nErgebnis der Addition lautet: ");
		 System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnisPl);
		 System.out.print("\n\n\nErgebnis der Subtraktion lautet: ");
		 System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnisMi);
		 System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
		 System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnisMa);
		 System.out.print("\n\n\nErgebnis der Division lautet: ");
		 System.out.print(zahl1 + " : " + zahl2 + " = " + ergebnisDu);
		 myScanner.close();
	}
}
