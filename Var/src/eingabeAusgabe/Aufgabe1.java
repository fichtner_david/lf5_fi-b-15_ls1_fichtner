package eingabeAusgabe;

import java.util.Scanner;

//David Fichtner
public class Aufgabe1 {

	public static void main(String[] args) {
	
  /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen. Vereinbaren Sie eine geeignete Variable */
		
		int zaehler=0;
		zaehler++; // am besten in einer Schleife
		
  /* 2. Weisen Sie dem Zaehler den Wert 25 zu
        und geben Sie ihn auf dem Bildschirm aus.*/
		zaehler = 25;
		System.out.println(zaehler);
		
  /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
        eines Programms ausgewaehlt werden.
        Vereinbaren Sie eine geeignete Variable */
		System.out.println("Geben sie A f�r About oder B f�r buch ein.");
		Scanner eingabe = new Scanner(System.in);
		
		//oder
		char eingabe2 = 'd';
		
  /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
        und geben Sie ihn auf dem Bildschirm aus.*/
		
		eingabe2 = 'C';
  /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
        notwendig.
        Vereinbaren Sie eine geeignete Variable */
		long zahl;

  /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
        und geben Sie sie auf dem Bildschirm aus.*/
		
		zahl = 299792458;
		System.out.println("Im luftleeren Raum betr�gt die Lichtgeschwindigkeit:"+zahl+"ms");
		
  /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
        soll die Anzahl der Mitglieder erfasst werden.
        Vereinbaren Sie eine geeignete Variable und initialisieren sie
        diese sinnvoll.*/
		byte mitglieder = 7;
		
  /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
		
		System.out.println("Anzahl der Mitglieder: "+mitglieder);

  /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
        Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
        dem Bildschirm aus.*/
		final double eleLadung = 1.602176634;
		System.out.println("elektrische Elementarladung: "+eleLadung);
		
  /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
        Vereinbaren Sie eine geeignete Variable. */
		boolean zahlungErfolgt;

  /*11. Die Zahlung ist erfolgt.
        Weisen Sie der Variable den entsprechenden Wert zu
        und geben Sie die Variable auf dem Bildschirm aus.*/
		zahlungErfolgt =true;
		System.out.println("Zahlung erfolgt:"+zahlungErfolgt);
	}
}
