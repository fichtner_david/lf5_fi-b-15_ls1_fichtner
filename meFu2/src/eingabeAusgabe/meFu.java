package eingabeAusgabe;
import java.util.Scanner;
public class meFu {
   
	public static void main(String[] args) {
		//Aufgabe 2
		double ergebnis = rechnung(2.36,7.87);
		System.out.println("Das ergebniss der Rechnugn ist: " + ergebnis);
		
		//Aufgabe 4
		System.out.println("!!!Bitte alle Angaben in cm!!!");
		System.out.println("W�rfel: " + wuerfel(eingabeWuerfel()) + "cm�");
		System.out.println("Quader: " + quader(eingabeQuaderA(),eingabeQuaderB(),eingabeQuaderC()) + "cm�");
		System.out.println("Pyramide: " + pyramide(eingabePyramideL(), eingabePyramideH()) + "cm�");
		System.out.println("Kugel: " + kugel(eingabeKugel()) + "cm�");
	}
	
	public static double rechnung( double x, double y) {

		double ergebnis= x * y;
		return ergebnis;
		
	}
	
	public static double wuerfel( double a) {

		double v = a * a * a;
		return v;
		
	}
	
	public static double eingabeWuerfel() {

		Scanner myScanner = new Scanner(System.in);
		System.out.println("W�RFEL: L�nge einer Seite angeben");
		double x = myScanner.nextDouble();
		return x;
		
	}
	
	public static double quader( double a, double b, double c) {

		double v = a * b * c;
		return v;
		
	}
	
	public static double eingabeQuaderA() {

		Scanner myScanner = new Scanner(System.in);
		System.out.println("QUADER: L�nge der ersten Seite angeben");
		double x = myScanner.nextDouble();
		return x;
		
	}
	
	public static double eingabeQuaderB() {

		Scanner myScanner = new Scanner(System.in);
		System.out.println("QUADER: L�nge der zweiten Seite angeben");
		double x = myScanner.nextDouble();
		return x;
		
	}
	
	public static double eingabeQuaderC() {

		Scanner myScanner = new Scanner(System.in);
		System.out.println("QUADER: L�nge der dritten Seite angeben");
		double x = myScanner.nextDouble();
		return x;
		
	}
	
	public static double pyramide( double a, double h) {
		
		double f = h / 3;
		double v = a * a * f;
		return v;
		
	}
	
	public static double eingabePyramideL() {

		Scanner myScanner = new Scanner(System.in);
		System.out.println("PYRAMIDE: L�nge angeben");
		double x = myScanner.nextDouble();
		return x;
		
	}
	
	public static double eingabePyramideH() {

		Scanner myScanner = new Scanner(System.in);
		System.out.println("PYRAMIDE: H�he angeben");
		double x = myScanner.nextDouble();
		return x;
		
	}
	
	public static double kugel( double r) {
		
		double f = 4.0/3.0;
		double g = r * r * r;
		double v = f * g * Math.PI;
		return v;
		
	}
	
	public static double eingabeKugel() {

		Scanner myScanner = new Scanner(System.in);
		System.out.println("KUGEL: Radius eingeben");
		double x = myScanner.nextDouble();
		return x;
		
	}
}
