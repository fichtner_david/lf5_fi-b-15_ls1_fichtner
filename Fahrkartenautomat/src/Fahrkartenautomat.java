﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args){
      
       double zuZahlenderBetrag; 
       double rückgabebetrag;
       //A2.5  5. ich habe int gewählt, da dies der gängigste Variablentyp für ganze Zahlen ist.
       zuZahlenderBetrag= fahrkartenbestellungErfassen();

       // Geldeinwurf
       // -----------
       double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       rueckgeldAusgeben(rückgabebetrag);

    }
    public static double fahrkartenbestellungErfassen() {

    	Scanner tastatur = new Scanner(System.in);
    	System.out.print("Zu zahlender Betrag (EURO): ");
        double zuZahlenderBetrag = tastatur.nextDouble();
        if (zuZahlenderBetrag <0) {
        	System.out.println("Die eingegebene Preis ist im negativen Bereich! Der Vorgang wird abgebrochen.");
        	System.exit(1);
        }
        
        System.out.print("Ticketanzahl?: ");
        int ticketanzahl = tastatur.nextInt();
        if (ticketanzahl <1 || ticketanzahl > 10) {
        	System.out.println("Die eingegebene Menge ist nicht gültig! Die Anzahl wird auf 1 gesetzt.");
        	ticketanzahl=1;
        }
        
        
        zuZahlenderBetrag= zuZahlenderBetrag * ticketanzahl;
        return zuZahlenderBetrag;
		
	}
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {

    	double eingezahlterGesamtbetrag;
    	Scanner tastatur = new Scanner(System.in);
    	eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag){
     	   System.out.println("Noch zu zahlen: " + String.format("%.2f", zuZahlenderBetrag - eingezahlterGesamtbetrag) + " EURO");
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        tastatur.close();
		return eingezahlterGesamtbetrag;
		
	}
    public static void fahrkartenAusgeben() {

    	System.out.println("\nFahrschein wird ausgegeben");
        warten(250);
        System.out.println("\n\n");
		
	}
    public static void warten(int millisekunde) {

        for (int i = 0; i < 8; i++){
           System.out.print("=");
           try {
 			Thread.sleep(millisekunde);
 		} catch (InterruptedException e) {
 			e.printStackTrace();
 		}
        }
		
	}
    public static double rueckgeldAusgeben(double rueckgabebetrag) {

    	if(rueckgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " +String.format("%.2f", rueckgabebetrag) + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rueckgabebetrag -= 2.0;
            }
            while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rueckgabebetrag -= 1.0;
            }
            while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rueckgabebetrag -= 0.2;
            }
            while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rueckgabebetrag -= 0.1;
            }
            while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rueckgabebetrag -= 0.05;
            }
            
        }
    	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
		return rueckgabebetrag;
		
	}
    public static void muenzeAusgeben(int rueckgabebetrag, String einheit) {

    	if(rueckgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " +String.format("%.2f", rueckgabebetrag) + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 " + einheit);
 	          rueckgabebetrag -= 2.0;
            }
            while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 " + einheit);
 	          rueckgabebetrag -= 1.0;
            }
            einheit="Cent";
            while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 " + einheit);
 	          rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 " + einheit);
  	          rueckgabebetrag -= 0.2;
            }
            while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 " + einheit);
 	          rueckgabebetrag -= 0.1;
            }
            while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 " + einheit);
  	          rueckgabebetrag -= 0.05;
            }
            
        }
    	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
		
	}
}