import java.util.Scanner;

public class AufgabeBMI {

	public static void main(String[] args) {
		Scanner rein = new Scanner(System.in);
		System.out.println("Was ist Ihr Körpergewicht?(In kg)");
		double gewicht = rein.nextDouble();
		System.out.println("Was ist Ihre Körpergröße?(in cm)");
		double groesse = rein.nextDouble();
		groesse = groesse /100;
		System.out.println("Was ist Ihr Geschlecht?(m/w)");
		char sex =rein.next().charAt(0);
		double bmi = 0;
		switch(sex) {
		case 'm': bmi = gewicht/groesse;
				  if(bmi<20) {
					  System.out.println("Du hast Untergewicht!");
				  }else {
					  if(bmi<25) {
						  System.out.println("Du hast Normalgewicht!");
					  }else {
							  System.out.println("Du hast Übergewicht!");
					  }
				  }
				  break;
		case 'w': bmi = gewicht/groesse;
				  if(bmi<29) {
					  System.out.println("Du hast Untergewicht!");
				  }else {
					  if(bmi<24) {
						  System.out.println("Du hast Normalgewicht!");
					  }else {
						  System.out.println("Du hast Übergewicht!");
					  }
				  }
				  break;
		}
		rein.close();
}
}
